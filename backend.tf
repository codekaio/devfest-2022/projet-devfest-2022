terraform {
  backend "gcs" {
    bucket = "terraform-state-projet-devfest-2022"
    prefix = "state"
  }
}
